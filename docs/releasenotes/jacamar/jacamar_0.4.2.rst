Jacamar CI v0.4.2
=================

* *Release*: `v0.4.2 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.4.2>`_
* *Date*: 02/24/2021

Admin Changes
-------------

* When using the supported RPM, ``jacamar-auth`` application is now deployed
  to ``/opt/jacamar/bin`` to best support ``CAP_SETUID``/``CAP_SETGID``
  workflows
  (`!97 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/97>`_)

  - Any previous deployments of Jacamar should be completely removed
    before deploying this latest version to avoid potential conflict.

  - The ``/opt/jacamar`` directory is protected with ``700``
    permissions and owned by default by ``root``.

* Configurable ``root_dir_creation = true`` in Jacamar-Auth supports
  creation of top level user owned directory by a privileged user
  (`!99 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/99>`_).

* Support for Runner versions 13.8 and removal of federation patch
  (`!96 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/96>`_).

Bug & Development Fixes
-----------------------

* Target server host for script augmentation of target URLs
  (`!98 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/98>`_).

  - Instances variables, such as ``CI_SERVER_HOST``, where not
    previously being updated correctly when the broker was enabled.

* Improved static analysis and linting during CI process
  (`!95 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/95>`_,
  `!94 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/94>`_).
