Jacamar CI v0.8.1
=================

* *Release*: `v0.8.1 <https://gitlab.com/ecp-ci/jacamar-ci/-/releases/v0.8.1>`_
* *Date*: 08/12/2021

.. important::

    With the `0.8.0` release we no longer require the usage of a patched
    runner. Instead you will need to use the
    `official release <https://docs.gitlab.com/runner/install/>`_, version
    `14.1+` for Jacamar CI to function correctly. Updating Jacamar CI will
    necessitate an update of the runner in conjunction to continue support.

Admin Changes
-------------

* Improved ``sudo`` downscope option and target command directory
  (`!238 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/238>`_). -
  @joe-snyder

Bug & Development Fixes
-----------------------

* Commands using ``qstat`` properly observer default timeout or configured
  ``command_delay`` (`!239 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/239>`_).
* Basic ``sudo`` integration testing via Pavilion
  (`!236 <https://gitlab.com/ecp-ci/jacamar-ci/-/merge_requests/236>`_).
