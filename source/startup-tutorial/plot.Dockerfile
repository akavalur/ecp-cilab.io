FROM debian:10-slim

RUN apt-get update \
    && apt-get install -y \
        gnuplot \
        make \
        python \
        python3-matplotlib \
        wget \
    && rm -rf /var/lib/apt/lists/*
