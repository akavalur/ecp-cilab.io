#!/usr/bin/env python3

import os
import requests

# In order to improve the readability of this example we've seperated
# obtaining the target environment variables from the request.
# Note that any variables prefixed by "BUILDSTATUS_" are established
# by the project maintainer, else they are provided by the runner.

project = os.getenv('BUILDSTATUS_PROJECT')
api = os.getenv('BUILDSTATUS_APIURL')
token = os.getenv('BUILDSTATUS_TOKEN')
name = os.getenv('BUILDSTATUS_JOB')

sha = os.getenv('CI_COMMIT_SHA')
state = os.getenv('CI_JOB_NAME')
url = os.getenv('CI_PIPELINE_URL')

# https://docs.gitlab.com/ee/api/commits.html#post-the-build-status-to-a-commit
# Load and identify requirement from environment.
status = {
    'id': project,
    'sha': sha,
    'state': state,
    'name': name,
    'target_url': url
}
r = requests.post("{}/projects/{}/statuses/{}".format(api, project, sha),
                headers={'PRIVATE-TOKEN': token},
                data=status)
if r.status_code != 201:
    print(r.text)
    exit(1)
